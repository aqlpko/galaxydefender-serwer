#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    openSession();

    connect(server,SIGNAL(newConnection()),this,SLOT(newConnection()));
    connect(&timer,SIGNAL(timeout()),this,SLOT(timerTick()));

    timer.setInterval(25);
    timer.setSingleShot(false);

    gracz1=false;

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openSession()
{
    server = new QTcpServer(this);
    if (!server->listen())
    {
        close();
        return;
    }
    QString ipAddress;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    for (int i = 0; i < ipAddressesList.size(); ++i)
    {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost
                &&ipAddressesList.at(i).toIPv4Address())
        {
            ipAddress = ipAddressesList.at(i).toString();
            break;
        }
    }
    if (ipAddress.isEmpty())
        ipAddress = QHostAddress(QHostAddress::LocalHost).toString();

    ui->statusLabel->setText("IP: "+ipAddress+" port: "+QString::number(server->serverPort()));
}

void MainWindow::newConnection()
{
    if(!gracz1)
    {
        Gracz1 = server->nextPendingConnection();
        connect(Gracz1,SIGNAL(readyRead()),this,SLOT(newMessage1()));
        ui->logWindow->insertPlainText("Connected Gracz1");
        gracz1=true;
    }
    else
    {
        Gracz2 = server->nextPendingConnection();
        connect(Gracz2,SIGNAL(readyRead()),this,SLOT(newMessage2()));
        ui->logWindow->insertPlainText("Connected Gracz2");
        gra=new Gra();
        gra->start();
        timer.start();
    }
}

void MainWindow::timerTick()
{
    QTextStream out1(Gracz1);
    QTextStream out2(Gracz2);
    QString message;
    if(gra->koniec())
    {
        timer.stop();
        out1<<(QString)"4\n";
        out2<<(QString)"4\n";
    }
    gra->kolizja();
    gra->usun();
    gra->poziom();
    gra->move();
    message=gra->dane();
    out1<<message;
    out2<<message;
}

void MainWindow::newMessage1()
{
        if (Gracz1->isReadable() && Gracz1->bytesAvailable()>0)
        {
            QTextStream in(Gracz1);
            QString message = in.readAll();
            QString liczba;
            for(int i=0;i<message.size();i++)
            {
                liczba=message[i];
                if(liczba.toInt()==5)
                    gra->dodajPocisk1();
                else
                    gra->set_v1(liczba.toInt());
            }
        }
}

void MainWindow::newMessage2()
{
        if (Gracz2->isReadable() && Gracz2->bytesAvailable()>0)
        {
            QTextStream in(Gracz2);
            QString message = in.readAll();
            QString liczba;
            for(int i=0;i<message.size();i++)
            {
                liczba=message[i];
                if(liczba.toInt()==5)
                    gra->dodajPocisk2();
                else
                    gra->set_v2(liczba.toInt());
            }
        }
}

void MainWindow::on_pushButton_clicked()
{
    timer.stop();
    Gracz1->abort();
    Gracz2->abort();
    gracz1=false;
}
