#-------------------------------------------------
#
# Project created by QtCreator 2014-06-01T23:28:28
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GalaxyDefender-Serwer
TEMPLATE = app


SOURCES += \
    main.cpp \
    mainwindow.cpp \
    mechanika.cpp \
    obiekt.cpp

HEADERS  += \
    mainwindow.h \
    mechanika.h \
    obiekt.h

FORMS    += \
    mainwindow.ui
