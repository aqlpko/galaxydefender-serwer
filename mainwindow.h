#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork>
#include <QTimer>
#include "mechanika.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    bool gracz1;
    QTcpServer* server;
    QTcpSocket* Gracz1;
    QTcpSocket* Gracz2;
    QTimer timer;
    Gra* gra;
private slots:
    void openSession();
    void newConnection();
    void timerTick();
    void newMessage1();
    void newMessage2();
    void on_pushButton_clicked();
};

#endif // MAINWINDOW_H
