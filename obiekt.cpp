#include "Obiekt.h"

Obiekt::Obiekt()
{
    pos[0]=0;
    pos[1]=0;
    v[0]=0;
    v[1]=0;
    r=0;
    usun=false;
}
Obiekt::Obiekt(float* pos_)
{
    pos[0]=pos_[0];
    pos[1]=pos_[1];
    usun=false;
}
Obiekt::~Obiekt(){}
float* Obiekt::get_pos()
{
    return pos;
}
float Obiekt::get_r()
{
    return r;
}
bool Obiekt::get_usun()
{
    return usun;
}
Pocisk::Pocisk():Obiekt()
{
    dmg=0;
    color[0]=0;
    color[1]=0;
    color[2]=0;
    color[3]=0;
}
Pocisk::Pocisk(float* pos_, float r_, int kierunek, float *color_):Obiekt(pos_)
{
    r=r_;
    color[0]=color_[0];
    color[1]=color_[1];
    color[2]=color_[2];
    color[3]=color_[3];
    v[0]=0;
    v[1]=r*2.1*kierunek;
    dmg=r*11000;
}
Pocisk::~Pocisk(){}
void Pocisk::move()
{
    if(v[1]>0)
    {
        if(pos[1]+r>1)
        {
            v[0]=0;
            v[1]=0;
            usun=true;
        }
        pos[0]+=v[0];
        pos[1]+=v[1];
    }
    else
    {
        if(pos[1]-r<0)
        {
            v[0]=0;
            v[1]=0;
            usun=true;
        }
        pos[0]+=v[0];
        pos[1]+=v[1];
    }
}
QString Pocisk::get_info()
{
    QString info="";
    QString spacja=" ";
    info+=QString::number(r)+spacja;
    info+=QString::number(pos[0])+spacja+QString::number(pos[1])+spacja;
    info+=QString::number(color[0])+spacja+QString::number(color[1])+spacja+QString::number(color[2])+spacja+QString::number(color[3])+(QString)"\n";
    return info;
}
float* Pocisk::get_dane_kolizja()
{
    float* dane=new float[3];//x, y1, y2
    dane[0]=pos[0];
    dane[1]=pos[1];
    dane[2]=pos[1]+v[1];
    return dane;
}

int Pocisk::get_dmg()
{
    return dmg;
}
void Pocisk::kolizja()
{
    usun=true;
}
bool Pocisk::zwrot()
{
    if(v[1]>0)
        return true;
    else
        return false;
}

Wrog::Wrog():Obiekt()
{
    hp=0;
    pocisk=false;
    licznik=0;
}

Wrog::Wrog(float* pos_):Obiekt(pos_)
{
    pocisk=false;
    licznik=0;
}
Wrog::~Wrog(){}

QString Wrog::get_info()
{
    QString info="";
    QString spacja=" ";
    info+=QString::number(r)+spacja;
    info+=QString::number(pos[0])+spacja+QString::number(pos[1])+(QString)"\n";
    return info;
}
float* Wrog::get_dane_kolizja()
{
    float* dane=new float[4];//y1, y2, x2, x3
    dane[0]=pos[1]-r;
    dane[1]=pos[1]+r;
    dane[2]=pos[0]+r;
    dane[3]=pos[0]-r;
    return dane;
}

bool Wrog::kolizja(int dmg)
{
    hp-=dmg;
    if(hp<=0)
    {
        usun=true;
        return true;
    }
    return false;
}
bool Wrog::get_pocisk()
{
    return pocisk;
}

Wrog1::Wrog1():Wrog(){}
Wrog1::Wrog1(float* pos_):Wrog(pos_)
{
    v[0]=0;
    v[1]=-0.005;
    r=0.035;
    hp=100;
}
Wrog1::~Wrog1(){}
void Wrog1::move()
{
    if(pos[1]-r<0)
    {
        v[1]=0;
        usun=true;
    }

    pos[0]+=v[0];
    pos[1]+=v[1];

    licznik++;
    if(licznik%80==0)
        pocisk=true;
    else
        pocisk=false;
}
Wrog2::Wrog2():Wrog(){}
Wrog2::Wrog2(float* pos_):Wrog(pos_)
{
    v[0]=0.003;
    v[1]=-0.004;
    r=0.044;
    hp=250;
}
Wrog2::~Wrog2(){}
void Wrog2::move()
{
    if(pos[1]-r<0)
    {
        v[1]=0;
        usun=true;
    }
    if(pos[0]-r+v[0]<0 || pos[0]+r+v[0]>0.75)
        v[0]*=-1;
    pos[0]+=v[0];
    pos[1]+=v[1];

    licznik++;
    if(licznik%60==0)
        pocisk=true;
    else
        pocisk=false;
}
Wrog3::Wrog3():Wrog(){}
Wrog3::Wrog3(float* pos_):Wrog(pos_)
{
    v[0]=0.002;
    v[1]=-0.003;
    r=0.058;
    hp=500;
}
Wrog3::~Wrog3(){}
void Wrog3::move()
{
    if(pos[1]-r<0)
    {
        v[1]=0;
        usun=true;
    }
    if(pos[0]-r+v[0]<0 || pos[0]+r+v[0]>0.75)
        v[0]*=-1;
    pos[0]+=v[0];
    pos[1]+=v[1];

    licznik++;
    if(licznik%40==0)
        pocisk=true;
    else
        pocisk=false;
}
Gracz::Gracz():Obiekt()
{
    hp=0;
    v_max[0]=0;
    v_max[1]=0;
    freq=0;
}
Gracz::Gracz(float* pos_):Obiekt(pos_)
{
    r=0.03;
    hp=2000;
    v_max[0]=0.006;
    v_max[1]=0.006;
    freq=2;
}
Gracz::~Gracz(){}
void Gracz::move()
{
    if(v[1]>0)
    {
        if(pos[1]+r+v[1]<1)
            pos[1]+=v[1];
        else
            v[1]=0;
    }
    else
    {
        if(pos[1]-r+v[1]>0)
            pos[1]+=v[1];
        else
            v[1]=0;
    }
    if(v[0]>0)
    {
        if(pos[0]+r+v[0]<0.75)
            pos[0]+=v[0];
        else
            v[0]=0;
    }
    else
    {
        if(pos[0]-r+v[0]>0)
            pos[0]+=v[0];
        else
            v[0]=0;
    }
    v[0]*=0.9;
    v[1]*=0.9;
}
void Gracz::x_increase()
{
    if(v[0]<v_max[0])
    v[0]+=0.0012;
}
void Gracz::x_decrease()
{
    if(v[0]>v_max[0]*-1)
    v[0]-=0.0012;
}
void Gracz::y_increase()
{
    if(v[1]<v_max[1])
    v[1]+=0.0012;
}
void Gracz::y_decrease()
{
    if(v[1]>v_max[1]*-1)
    v[1]-=0.0012;
}
QString Gracz::get_info()
{
    QString info="";
    QString spacja=" ";
    info+=QString::number(r)+spacja;
    info+=QString::number(pos[0])+spacja+QString::number(pos[1])+spacja;
    info+=QString::number(hp)+(QString)"\n";
    return info;
}
int Gracz::get_freq()
{
    return freq;
}
float* Gracz::get_dane_kolizja()
{
    float* dane=new float[4];//y1, y2, x2, x3
    dane[0]=pos[1]+r;
    dane[1]=pos[1]-r;
    dane[2]=pos[0]+r;
    dane[3]=pos[0]-r;
    return dane;
}
void Gracz::kolizja(int dmg)
{
    hp-=dmg;
    if(hp<=0)
        usun=true;
}
void Gracz::set_r(float val)
{
    r=val;
}
void Gracz::set_hp()
{
    hp=2000;
}
