#ifndef MECHANIKA_H
#define MECHANIKA_H

#include "obiekt.h"
#include <QList>

class Gra
{
protected:
    QList<Pocisk*> pocisk;
    QList<Wrog1*> wrog1;
    QList<Wrog2*> wrog2;
    QList<Wrog3*> wrog3;
    Gracz* gracz1;
    Gracz* gracz2;
    int licznik;
    int last_shot1;
    int last_shot2;
    int poz;
    int punkty;
    int awans;
public:
    Gra();
    ~Gra();
    void move();
    void dodajPocisk(float r,float* pos);
    void dodajPocisk1();
    void dodajPocisk2();
    void dodajWrog1();
    void dodajWrog2();
    void dodajWrog3();
    void start();
    void poziom();
    void poziom1();
    void poziom2();
    void poziom3();
    void set_v1(int val);
    void set_v2(int val);
    QString dane();
    void kolizja();
    void usun();
    bool koniec();
};

#endif // MECHANIKA_H
