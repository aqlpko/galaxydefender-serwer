#ifndef STANGRY_H
#define STANGRY_H

#include <QString>

class Obiekt
{
protected:
    float v[2];
    float pos[2];
    float r;
    bool usun;
public:
    Obiekt();
    Obiekt(float* pos_);
    virtual ~Obiekt();
    virtual void move()=0;
    virtual QString get_info()=0;
    float* get_pos();
    float get_r();
    bool get_usun();
};

class Pocisk: public Obiekt
{
    int dmg;
    float color[4];
public:
    Pocisk();
    Pocisk(float* pos_,float r,int kierunek, float* color_);
    ~Pocisk();
    virtual void move();
    virtual QString get_info();
    float* get_dane_kolizja();
    int get_dmg();
    void kolizja();
    bool zwrot();
};

class Wrog: public Obiekt
{
protected:
    int hp;
    bool pocisk;
    int licznik;
public:
    Wrog();
    Wrog(float* pos_);
    virtual ~Wrog();
    virtual QString get_info();
    float* get_dane_kolizja();
    bool kolizja(int dmg);
    bool get_pocisk();
};

class Wrog1: public Wrog
{
public:
    Wrog1();
    Wrog1(float* pos_);
    ~Wrog1();
    virtual void move();
};

class Wrog2: public Wrog
{
public:
    Wrog2();
    Wrog2(float* pos_);
    ~Wrog2();
    virtual void move();
};

class Wrog3: public Wrog
{
public:
    Wrog3();
    Wrog3(float* pos_);
    ~Wrog3();
    virtual void move();
};

class Gracz: public Obiekt
{
    int hp;
    float v_max[2];
    int freq;
public:
    Gracz();
    Gracz(float* pos_);
    ~Gracz();
    virtual void move();
    void x_increase();
    void x_decrease();
    void y_increase();
    void y_decrease();
    virtual QString get_info();
    float* get_dane_kolizja();
    int get_freq();
    void kolizja(int dmg);
    void set_r(float val);
    void set_hp();
};

#endif // STANGRY_H
