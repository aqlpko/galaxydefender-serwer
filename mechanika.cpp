#include "mechanika.h"
#include <QTime>

Gra::Gra()
{
    licznik=-200;
    last_shot1=0;
    last_shot2=0;
    poz=1;
    punkty=0;
    awans=2;
}
Gra::~Gra(){}
void Gra::move()
{
    for(int i=0;i<pocisk.size();i++)
        pocisk[i]->move();
    for(int i=0;i<wrog1.size();i++)
        wrog1[i]->move();
    for(int i=0;i<wrog2.size();i++)
        wrog2[i]->move();
    for(int i=0;i<wrog3.size();i++)
        wrog3[i]->move();
    gracz1->move();
    gracz2->move();
}
void Gra::dodajPocisk(float r,float* pos)
{
    float p[2];
    p[0]=pos[0];
    p[1]=pos[1];
    float color[4];
    color[3]=0;

    switch((int)(r*1100))
    {
    case 5:
    {
        color[0]=0;
        color[1]=0.67;
        color[2]=1;
        Pocisk* nowy=new Pocisk(p,r,-1,color);
        pocisk.append(nowy);
        break;
    }
    case 7:
    {
        color[0]=1;
        color[1]=0.78;
        color[2]=0;
        p[0]-=1.6*r;
        Pocisk* nowy=new Pocisk(p,r,-1,color);
        pocisk.append(nowy);
        p[0]+=3.2*r;
        nowy=new Pocisk(p,r,-1,color);
        pocisk.append(nowy);
        break;
    }
    case 9:
    {
        color[0]=0.63;
        color[1]=1;
        color[2]=0;
        Pocisk* nowy=new Pocisk(p,r,-1,color);
        pocisk.append(nowy);
        p[0]-=2.3*r;
        nowy=new Pocisk(p,r,-1,color);
        pocisk.append(nowy);
        p[0]+=4.6*r;
        nowy=new Pocisk(p,r,-1,color);
        pocisk.append(nowy);
        break;
    }
    }
}
void Gra::dodajPocisk1()
{
    float color[4];
    color[3]=0;
    if(licznik-last_shot1!=0 && 40/(licznik-last_shot1)<=gracz1->get_freq())
    {
    float r=gracz1->get_r();
    float* pos=gracz1->get_pos();
    float p[2];
    p[0]=pos[0];
    p[1]=pos[1];
    switch((int)(r*120))
    {
    case 3:
    {
        color[0]=0;
        color[1]=0.67;
        color[2]=1;
        Pocisk* nowy=new Pocisk(p,0.005,1,color);
        pocisk.append(nowy);
        break;
    }
    case 4:
    {
        color[0]=1;
        color[1]=0.78;
        color[2]=0;
        p[0]-=0.25*r;
        Pocisk* nowy=new Pocisk(p,0.007,1,color);
        pocisk.append(nowy);
        p[0]+=0.5*r;
        nowy=new Pocisk(p,0.007,1,color);
        pocisk.append(nowy);
        break;
    }
    case 5:
    {
        color[0]=0.63;
        color[1]=1;
        color[2]=0;
        Pocisk* nowy=new Pocisk(p,0.009,1,color);
        pocisk.append(nowy);
        p[0]-=0.35*r;
        nowy=new Pocisk(p,0.009,1,color);
        pocisk.append(nowy);
        p[0]+=0.7*r;
        nowy=new Pocisk(p,0.009,1,color);
        pocisk.append(nowy);
        break;
    }
    }
    last_shot1=licznik;
    }
}
void Gra::dodajPocisk2()
{
    float color[4];
    color[3]=0;
    if(licznik-last_shot2!=0 && 40/(licznik-last_shot2)<=gracz2->get_freq())
    {
    float r=gracz2->get_r();
    float* pos=gracz2->get_pos();
    float p[2];
    p[0]=pos[0];
    p[1]=pos[1];
    switch((int)(r*120))
    {
    case 3:
    {
        color[0]=0;
        color[1]=0.78;
        color[2]=1;
        Pocisk* nowy=new Pocisk(p,0.005,1,color);
        pocisk.append(nowy);
        break;
    }
    case 4:
    {
        color[0]=1;
        color[1]=0.78;
        color[2]=0;
        p[0]-=0.25*r;
        Pocisk* nowy=new Pocisk(p,0.007,1,color);
        pocisk.append(nowy);
        p[0]+=0.5*r;
        nowy=new Pocisk(p,0.007,1,color);
        pocisk.append(nowy);
        break;
    }
    case 5:
    {
        color[0]=0.63;
        color[1]=1;
        color[2]=0;
        Pocisk* nowy=new Pocisk(p,0.009,1,color);
        pocisk.append(nowy);
        p[0]-=0.35*r;
        nowy=new Pocisk(p,0.009,1,color);
        pocisk.append(nowy);
        p[0]+=0.7*r;
        nowy=new Pocisk(p,0.009,1,color);
        pocisk.append(nowy);
        break;
    }
    }
    last_shot2=licznik;
    }
}
void Gra::dodajWrog1()
{
    qsrand((uint)QTime::currentTime().msec());
    float pos_[2]={(float)(qrand()%65/100.+0.05),1.};
    Wrog1* nowy=new Wrog1(pos_);
    wrog1.append(nowy);
}
void Gra::dodajWrog2()
{
    qsrand((uint)QTime::currentTime().msec());
    float pos_[2]={(float)(qrand()%65/100.+0.05),1.};
    Wrog2* nowy=new Wrog2(pos_);
    wrog2.append(nowy);
}
void Gra::dodajWrog3()
{
    qsrand((uint)QTime::currentTime().msec());
    float pos_[2]={(float)(qrand()%65/100.+0.05),1.};
    Wrog3* nowy=new Wrog3(pos_);
    wrog3.append(nowy);
}
void Gra::start()
{
    float pos1[2]={0.2,0.1};
    float pos2[2]={0.55,0.1};
    gracz1=new Gracz(pos1);
    gracz2=new Gracz(pos2);
}
void Gra::poziom()
{
    switch(poz)
    {
    case 1:
    {
        poziom1();
        break;
    }
    case 2:
    {
        poziom2();
        break;
    }
    case 3:
    {
        poziom3();
        break;
    }
    }
    if(awans==2 && punkty>300 && gracz1->get_r()<0.35)
    {
        gracz1->set_r(0.036);
        gracz2->set_r(0.036);
        gracz1->set_hp();
        gracz2->set_hp();
        awans=3;

    }
    if(awans==3 && punkty>1000 && gracz1->get_r()<0.42)
    {
        gracz1->set_r(0.043);
        gracz2->set_r(0.043);
        gracz1->set_hp();
        gracz2->set_hp();
        awans=4;
    }
}

void Gra::poziom1()
{
    licznik++;
    if(licznik>0 && licznik%80==0)
        dodajWrog1();
    if(licznik>600 && licznik%83==0)
        dodajWrog1();
    if(licznik>1000 && licznik%147==0)
        dodajWrog1();
    if(licznik>1300 && licznik%179==0)
        dodajWrog1();

    for(int i=0;i<wrog1.size();i++)
        if(wrog1[i]->get_pocisk())
            dodajPocisk(0.005,wrog1[i]->get_pos());
    for(int i=0;i<wrog2.size();i++)
        if(wrog2[i]->get_pocisk())
            dodajPocisk(0.007,wrog2[i]->get_pos());
    for(int i=0;i<wrog3.size();i++)
        if(wrog3[i]->get_pocisk())
            dodajPocisk(0.009,wrog3[i]->get_pos());


    if(licznik>1500)
    {
        poz=2;
        licznik=-200;
        last_shot1=0;
        last_shot2=0;
    }
}
void Gra::poziom2()
{
    licznik++;
    if(licznik>0 && licznik%80==0)
        dodajWrog1();
    if(licznik>200 && licznik%83==0)
        dodajWrog2();
    if(licznik>700 && licznik%152==0)
        dodajWrog1();
    if(licznik>1000 && licznik%179==0)
        dodajWrog2();
    if(licznik>1400 && licznik%209==0)
        dodajWrog1();
    if(licznik>1800 && licznik%254==0)
        dodajWrog2();

    for(int i=0;i<wrog1.size();i++)
        if(wrog1[i]->get_pocisk())
            dodajPocisk(0.005,wrog1[i]->get_pos());
    for(int i=0;i<wrog2.size();i++)
        if(wrog2[i]->get_pocisk())
            dodajPocisk(0.007,wrog2[i]->get_pos());

    if(licznik>2100)
    {
        poz=3;
        licznik=-200;
        last_shot1=0;
        last_shot2=0;
    }
}
void Gra::poziom3()
{
    licznik++;
    if(licznik<5700)
    {
        if(licznik>0 && licznik%80==0)
            dodajWrog1();
        if(licznik>400 && licznik%83==0)
            dodajWrog2();
        if(licznik>700 && licznik%165==0)
            dodajWrog3();
        if(licznik>1300 && licznik%203==0)
            dodajWrog2();
        if(licznik>2000 && licznik%177==0)
            dodajWrog3();
        if(licznik>2800 && licznik%279==0)
            dodajWrog3();
        if(licznik>3700 && licznik%181==0)
            dodajWrog2();
        if(licznik>4800 && licznik%193==0)
            dodajWrog3();
    }

    for(int i=0;i<wrog1.size();i++)
        if(wrog1[i]->get_pocisk())
            dodajPocisk(0.005,wrog1[i]->get_pos());
    for(int i=0;i<wrog2.size();i++)
        if(wrog2[i]->get_pocisk())
            dodajPocisk(0.007,wrog2[i]->get_pos());
    for(int i=0;i<wrog3.size();i++)
        if(wrog3[i]->get_pocisk())
            dodajPocisk(0.009,wrog3[i]->get_pos());

    if(licznik>6000)
        poz=4;
}

void Gra::set_v1(int val)
{
    switch(val)
    {
    case 1:
    {
        gracz1->y_increase();
        break;
    }
    case 2:
    {
        gracz1->y_decrease();
        break;
    }
    case 3:
    {
        gracz1->x_increase();
        break;
    }
    case 4:
    {
        gracz1->x_decrease();
        break;
    }
    }
}
void Gra::set_v2(int val)
{
    switch(val)
    {
    case 1:
    {
        gracz2->y_increase();
        break;
    }
    case 2:
    {
        gracz2->y_decrease();
        break;
    }
    case 3:
    {
        gracz2->x_increase();
        break;
    }
    case 4:
    {
        gracz2->x_decrease();
        break;
    }
    }
}
QString Gra::dane()
{
    QString wynik="";

    if(licznik>0 && poz!=4)
        wynik+=QString::number(0);
    else
        wynik+=QString::number(poz);
    wynik+="\n";
    for(int i=0;i<pocisk.size();i++)
        wynik+=pocisk[i]->get_info();
    wynik+=".\n";
    for(int i=0;i<wrog1.size();i++)
        wynik+=wrog1[i]->get_info();
    wynik+=".\n";
    for(int i=0;i<wrog2.size();i++)
        wynik+=wrog2[i]->get_info();
    wynik+=".\n";
    for(int i=0;i<wrog3.size();i++)
        wynik+=wrog3[i]->get_info();
    wynik+=".\n";
    wynik+=gracz1->get_info();
    wynik+=gracz2->get_info();
    wynik+=QString::number(punkty);

    return wynik;
}
void Gra::usun()
{
    for(int i=0;i<pocisk.size();i++)
        if(pocisk[i]->get_usun())
        {
            delete pocisk[i];
            pocisk.takeAt(i);
        }
    for(int i=0;i<wrog1.size();i++)
        if(wrog1[i]->get_usun())
        {
            delete wrog1[i];
            wrog1.takeAt(i);
        }
    for(int i=0;i<wrog2.size();i++)
        if(wrog2[i]->get_usun())
        {
            delete wrog2[i];
            wrog2.takeAt(i);
        }
    for(int i=0;i<wrog3.size();i++)
        if(wrog3[i]->get_usun())
        {
            delete wrog3[i];
            wrog3.takeAt(i);
        }
}
void Gra::kolizja()
{
    float a1=0.5;
    float a2=-0.5;
    float b,y;
    float* d_w;
    float* d_p;
    float* d_g;

    for(int i=0;i<pocisk.size();i++)
    {
        d_p=pocisk[i]->get_dane_kolizja();
        if(pocisk[i]->zwrot())
        {
            for(int j=0;j<wrog1.size();j++)
            {                
                d_w=wrog1[j]->get_dane_kolizja();
                if(d_w[3]<=d_p[0] && d_w[2]>=d_p[0])
                {
                    b=d_w[1]-a1*d_w[2];
                    y=a1*d_p[0]+b;
                    if(d_p[2]>y && d_p[1]<y)
                    {
                        if(wrog1[j]->kolizja(pocisk[i]->get_dmg()))
                            punkty+=10;
                        pocisk[i]->kolizja();
                    }
                    else
                    {
                        b=d_w[1]-a2*d_w[3];
                        y=a2*d_p[0]+b;
                        if(d_p[2]>y && d_p[1]<y)
                        {
                            if(wrog1[j]->kolizja(pocisk[i]->get_dmg()))
                                punkty+=10;
                            pocisk[i]->kolizja();
                        }
                    }
                }
            }
            for(int j=0;j<wrog2.size();j++)
            {
                d_w=wrog2[j]->get_dane_kolizja();
                if(d_w[3]<=d_p[0] && d_w[2]>=d_p[0])
                {
                    b=d_w[1]-a1*d_w[2];
                    y=a1*d_p[0]+b;
                    if(d_p[2]>y && d_p[1]<y)
                    {
                        if(wrog2[j]->kolizja(pocisk[i]->get_dmg()))
                            punkty+=20;
                        pocisk[i]->kolizja();
                    }
                    else
                    {
                        b=d_w[1]-a2*d_w[3];
                        y=a2*d_p[0]+b;
                        if(d_p[2]>y && d_p[1]<y)
                        {
                            if(wrog2[j]->kolizja(pocisk[i]->get_dmg()))
                                punkty+=20;
                            pocisk[i]->kolizja();
                        }
                    }
                }
            }
            for(int j=0;j<wrog3.size();j++)
            {
                d_w=wrog3[j]->get_dane_kolizja();
                if(d_w[3]<=d_p[0] && d_w[2]>=d_p[0])
                {
                    b=d_w[1]-a1*d_w[2];
                    y=a1*d_p[0]+b;
                    if(d_p[2]>y && d_p[1]<y)
                    {
                        if(wrog3[j]->kolizja(pocisk[i]->get_dmg()))
                            punkty+=40;
                        pocisk[i]->kolizja();
                    }
                    else
                    {
                        b=d_w[1]-a2*d_w[3];
                        y=a2*d_p[0]+b;
                        if(d_p[2]>y && d_p[1]<y)
                        {
                            if(wrog3[j]->kolizja(pocisk[i]->get_dmg()))
                                punkty+=40;
                            pocisk[i]->kolizja();
                        }
                    }
                }
            }
        }
        else
        {
            d_g=gracz1->get_dane_kolizja();
            if(d_g[2]>=d_p[0] && d_g[3]<=d_p[0])
            {
                b=d_g[1]-a2*d_g[2];
                y=a2*d_p[0]+b;
                if(d_p[1]>y && d_p[2]<y)
                {
                    gracz1->kolizja(pocisk[i]->get_dmg());
                    pocisk[i]->kolizja();
                }
                else
                {
                    b=d_g[1]-a1*d_g[3];
                    y=a1*d_p[0]+b;
                    if(d_p[1]>y && d_p[2]<y)
                    {
                        gracz1->kolizja(pocisk[i]->get_dmg());
                        pocisk[i]->kolizja();
                    }
                }
            }

            d_g=gracz2->get_dane_kolizja();
            if(d_g[2]>=d_p[0] && d_g[3]<=d_p[0])
            {
                b=d_g[1]-a2*d_g[2];
                y=a2*d_p[0]+b;
                if(d_p[1]>y && d_p[2]<y)
                {
                    gracz2->kolizja(pocisk[i]->get_dmg());
                    pocisk[i]->kolizja();
                }
                else
                {
                    b=d_g[1]-a1*d_g[3];
                    y=a1*d_p[0]+b;
                    if(d_p[1]>y && d_p[2]<y)
                    {
                        gracz2->kolizja(pocisk[i]->get_dmg());
                        pocisk[i]->kolizja();
                    }
                }
            }
        }
    }
}
bool Gra::koniec()
{
    if(gracz1->get_usun() || gracz2->get_usun() || poz==4)
        return true;

    return false;
}
